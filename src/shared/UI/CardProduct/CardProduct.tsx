import './CardProduct.css';
interface Props {
    name: string;
    image: string;
    price: number;
    onClick: React.MouseEventHandler;
}

export const CardProduct = (props: Props) => {

    const { name, price, image, onClick } = props;

    return (
        <div onClick={onClick} className="cursor-pointer  me-5 mt-4 mb-3 flex-column justify-content-between  px-4 d-flex py-4 card-product">
            <div className="d-flex justify-content-center">
                <img className='rounded-circle'  width={180} height={180} src={image} alt="" />
            </div>
            <div>
                <div className="text-muted  h5 align-items-end  fw-bold mt-3  center">
                    <span>
                        { name }
                    </span>
                </div>
                <div className="price  mt-3  center">
                    <span>
                        Precio: ${price}
                    </span>
                </div>
            </div>
                    
        </div>
    );
}