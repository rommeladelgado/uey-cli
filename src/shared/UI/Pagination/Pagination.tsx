import { useContext, useEffect, useMemo, useState } from "react";
import { EventContext } from "../../../context/eventContext";
import './Pagination.css';

export const Pagination = () => {
    const {
        rowsPerPage,
        page,
        selectPage,
        total,
     } = useContext(EventContext);
     
     const [totalPages, setTotalPages] = useState(0);
     useEffect(() => {
        if (total) {
            setTotalPages(Math.floor(total / rowsPerPage));
        }
     }, [total, rowsPerPage]);

     const pages = useMemo(() => {
        if (totalPages < 3) {
            return Array(2).fill(0).map((_, index) => index + 1);
        } else if (page < 3) {
            return [1,2,3];
        } else {
            return [page, page + 1, page + 2];
        }
     }, [totalPages, page]);

     const previousPage = () => {

        if (page !== 0) {
            selectPage(page - 1);
        }
     };

     const nextPage = () => {
        if (page !== (totalPages)) {
            selectPage(page + 1);
        }
     };

    return (
        <div className="d-flex">
            <div onClick={() => previousPage()} className={`tab-pagination ${page === 0 && 'tab-disabled'}`}>
                <span>Anterior</span>
            </div>
            { pages.map((e, index) => (
                <div onClick={() => selectPage( index)} className={`tab-pagination ${index === page && 'tab-active'}`}>
                    <span>{e}</span>
                </div>
            )) }
            <div onClick={() =>  nextPage()} className={`tab-pagination ${page === (totalPages) && 'tab-disabled'}`}>
                <span>Siguiente</span>
            </div>
        </div>
    );
}

