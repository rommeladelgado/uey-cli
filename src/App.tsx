import React from 'react';
//import logo from './logo.svg';
import './App.css';
import { MainContainer } from './views/MainContainer/MainContainer';


function App() {
  return (
    <div className="">
      <MainContainer />
    </div>
  );
}

export default App;
