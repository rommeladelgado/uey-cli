import { createBrowserRouter } from 'react-router-dom'
import { MainContainer } from '../views/MainContainer/MainContainer';
import { ViewPlainProduct } from '../views/ViewProduct/components/ViewPlainProduct/ViewPlainProduct';
import { ViewProfitableTypeProduct } from '../views/ViewProduct/components/ViewProfitableTypeProduct/ViewProfitableTypeProduct';
import { ViewSpaceTypeProduct } from '../views/ViewProduct/components/ViewSpaceTypeProduct/ViewSpaceTypeProduct';


export const router = createBrowserRouter([
    { path: '/', Component: MainContainer },
    { path: '/product/plain-product/:productId', Component: ViewPlainProduct },
    { path: '/product/profitable-type-product/:productId', Component: ViewProfitableTypeProduct },
    { path: '/product/space-type-product/:productId', Component: ViewSpaceTypeProduct },
]);