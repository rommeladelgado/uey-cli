export type TypeRent = 'hour' | 'night';
export interface ProfitableReservation {
    reservationDate: string;
    typeRent: TypeRent;
}