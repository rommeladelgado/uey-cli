export interface SpaceReservation {
    id: number;
    reservationDate: string;
}