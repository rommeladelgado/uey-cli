import { ProfitableReservation } from "./profitableReservation";
import { SpaceReservation } from "./spaceReservation";

export type TypeProduct = 'simple' | 'arrendable' | 'space';

export enum TypeProductEnum {
    SIMPLE = 'simple',
    ARRENDABLE = 'arrendable',
    SPACE = 'space'
}

interface Product {
    id: number;
    price: number;
    name: string;
    seller:string;
    description: string;
    image: string;
}

export interface PlainProduct extends Product {
    stock: number;
}

export interface ProfitableTypeProduct extends Product {
    reservations: ProfitableReservation[];
}

export interface SpaceTypeProduct extends Product {
    coordinate: string;
    reservations: SpaceReservation[]
}