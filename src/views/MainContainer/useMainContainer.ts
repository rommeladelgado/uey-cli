import { useQuery } from "@apollo/client";
import { useEffect, useState } from "react";
import { PlainProduct, ProfitableTypeProduct, SpaceTypeProduct } from "../../interfaces/product";
import { TypeProduct } from "../../interfaces/product";
import { GET_ALL_PLAIN_PRODUCTS } from "../../queries/plainProduct";
import { GET_ALL_PROFITABLE_TYPE_PRODUCTS } from "../../queries/profitableTypeProduct";
import { GET_ALL_SPACE_TYPE_PRODUCTS } from "../../queries/spaceTypeProduct";


export const useMainContainer = () => {

    const [page, setPage] = useState(0);
    const [total, setTotal] = useState(0);
    const [typeProduct, setTypeProduct] = useState<TypeProduct>('simple');
    const [search, setSearch] = useState('');

    const [plainProducts, setPlainProducts] = useState<PlainProduct[]>([]);
    const [profitableTypeProducts, setProfitableTypeProducts] = useState<ProfitableTypeProduct[]>([]);
    const [spaceTypeProducts, setSpaceTypeProducts] = useState<SpaceTypeProduct[]>([]);

    const rowsPerPage = 8;

    const onChangeTypeProduct = (type: TypeProduct) => {
        setTypeProduct(type);
        setPage(0);
    };
    const onChangeSearch = (search: string) =>  setSearch(search);

    const { data: dataPlainProducts,  loading: loadingPlain } = useQuery(GET_ALL_PLAIN_PRODUCTS, {
        variables: { search: search, page: page, rowsPerPage: rowsPerPage }
    });
    

    const { data: dataSpaceTypeProducts,  loading: loadingSpaces } = useQuery(GET_ALL_SPACE_TYPE_PRODUCTS, {
        variables: { search: search, page: page, rowsPerPage: rowsPerPage }
    });

    const { data: dataProfitableTypeProducts,  loading: loadingProfitable } = useQuery(GET_ALL_PROFITABLE_TYPE_PRODUCTS, {
        variables: { search: search, page: page, rowsPerPage: rowsPerPage }
    });

    useEffect(() => {
        if (dataPlainProducts) {
            const { count, list } = dataPlainProducts.getAllPlainProducts;
            setTotal(count);
            setPlainProducts([...list]);
        }
    }, [dataPlainProducts]);

    useEffect(() => {
        if (dataSpaceTypeProducts) {
            const { count, list } = dataSpaceTypeProducts.getAllSpaceProducts;
            setTotal(count);
            setSpaceTypeProducts([...list]);
        }
    }, [dataSpaceTypeProducts])


    useEffect(() => {
        if (dataProfitableTypeProducts) {
            const { count, list } = dataProfitableTypeProducts.getAllProfitableProducts;
            setTotal(count);
            setProfitableTypeProducts([...list]);
        }
    }, [dataProfitableTypeProducts])
    return {
        page,
        loading: loadingPlain || loadingSpaces || loadingProfitable,
        total,
        rowsPerPage,
        typeProduct,
        search,
        plainProducts,
        profitableTypeProducts,
        spaceTypeProducts,
        selectPage: setPage,
        onChangeTypeProduct,
        onChangeSearch
    }
}