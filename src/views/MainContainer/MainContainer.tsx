import { EventProvider } from "../../context/EventProvider";
import { Body } from "./components/Body/Body";

import './MainContainer.css';
import { useMainContainer } from "./useMainContainer";
import {Header} from "./components/Header/Header";

export const MainContainer = () => {

    const { 
        page,
        loading,
        total,
        rowsPerPage,
        typeProduct,
        search,
        plainProducts,
        profitableTypeProducts,
        spaceTypeProducts,
        selectPage,
        onChangeTypeProduct,
        onChangeSearch
     } = useMainContainer();
    return (
        <EventProvider 
            page={page}
            loading={loading}
            total={total}
            rowsPerPage={rowsPerPage}
            typeProduct={typeProduct}
            search={search}
            plainProducts={plainProducts}
            profitableTypeProducts={profitableTypeProducts}
            spaceTypeProducts={spaceTypeProducts}
            selectPage={selectPage}
            onChangeTypeProduct={onChangeTypeProduct}

            onChangeSearch={onChangeSearch}
        >
            <div className="d-block justify-content-center ">
                <div className="w-100">
                    <Header />
                </div>
                <Body />
            </div>
        </EventProvider>
    );
}