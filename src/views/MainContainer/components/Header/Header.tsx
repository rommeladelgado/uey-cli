
import { CustomInput } from '../CustomInput/CustomInput';
import './Header.css';
export const Header = () => {

    return (
        <div className="header px-5 pt-3 pb-1 d-flex">
            <div className="h6 text-white me-5 mt-2">
                <span>Prueba UEY</span>
            </div>
            <CustomInput />
        </div>
    )
}