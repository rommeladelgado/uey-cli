import { useContext, useState, useEffect } from "react";
import "./CustomInput.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { EventContext } from "../../../../context/eventContext";
import { TypeProductEnum } from "../../../../interfaces/product";
export const CustomInput = () => {
  const { 
    onChangeTypeProduct,
    onChangeSearch, 
    search 
} = useContext(EventContext);

    const [text, setText] = useState(search);

    useEffect(() => {
      setText(search);
    }, [search])
    
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setText(event.target.value);
        
    };

    const handleKeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (event.key === 'Enter' ) {
        onChangeSearch(event.currentTarget.value);
      }
    }

    const handleChangeSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        onChangeTypeProduct(event.target.value);
    };
  return (
    <div className="w-75 d-flex">
      <div className="select-size ">
        <select onChange={handleChangeSelect} className="form-select text-muted ">
          <option value={TypeProductEnum.SIMPLE}>Productos simples</option>
          <option value={TypeProductEnum.ARRENDABLE}>Productos rentables</option>
          <option value={TypeProductEnum.SPACE}>Espacios</option>
        </select>
      </div>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control   "
          placeholder="Buscar producto"
          aria-label="Recipient's username"
          aria-describedby="button-addon2"
          value={text}
          onChange={handleChange}
          onKeyUp={handleKeyUp}
        />
        <button
          className="btn btn-input-search"
          type="button"
          id="button-addon2"
          onClick={() => onChangeSearch(text)}
        >
          <FontAwesomeIcon icon={faMagnifyingGlass} />
        </button>
      </div>
    </div>
  );
};
