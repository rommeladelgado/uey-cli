import {useContext} from "react";
import { useNavigate } from "react-router-dom";
import {EventContext, EventContextModel} from "../../../../context/eventContext";
import {PlainProduct, ProfitableTypeProduct, SpaceTypeProduct, TypeProductEnum} from "../../../../interfaces/product";
import {CardProduct} from "../../../../shared/UI/CardProduct/CardProduct";
import {Pagination} from "../../../../shared/UI/Pagination/Pagination";

export const Body = () => {

    const navigate = useNavigate();
    const {

        typeProduct,
        plainProducts,
        profitableTypeProducts,
        spaceTypeProducts,

    } = useContext<EventContextModel>(EventContext);

    const onItemPlainProduct = (item: PlainProduct) => {
        navigate(`/product/plain-product/${item.id}`);
    }

    const onItemProfitableTypeProduct = (item: ProfitableTypeProduct) => {
        navigate(`/product/profitable-type-product/${item.id}`);
    }

    const onItemSpaceTypeProduct = (item: SpaceTypeProduct) => {
        navigate(`/product/space-type-product/${item.id}`);
    }

    return (
        <div className="mt-4 d-flex justify-content-center">

            <div className="col-10">
                {typeProduct === TypeProductEnum.SIMPLE && (
                    plainProducts.map((item) => (
                        <div className="d-inline-block">
                            <CardProduct
                                key={item.id}
                                image={item.image}
                                name={item.name}
                                onClick={() => onItemPlainProduct(item)}
                                price={item.price}
                            />
                        </div>
                    )))
                }

                {typeProduct === TypeProductEnum.ARRENDABLE && (
                    profitableTypeProducts.map((item) => (
                        <div className="d-inline-block">
                            <CardProduct
                                key={item.id}
                                image={item.image}
                                name={item.name}
                                onClick={() => onItemProfitableTypeProduct(item)}
                                price={item.price}
                            />
                        </div>
                    )))
                }

                {typeProduct === TypeProductEnum.SPACE && (
                    spaceTypeProducts.map((item) => (
                        <div className="d-inline-block">
                            <CardProduct
                                key={item.id}
                                image={item.image}
                                name={item.name}
                                onClick={() => onItemSpaceTypeProduct(item)}
                                price={item.price}
                            />
                        </div>
                    )))
                }
                <Pagination/>
            </div>
        </div>
    );
}