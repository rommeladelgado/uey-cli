import 'react-calendar/dist/Calendar.css';
import { Calendar } from "react-calendar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import './Product.css';
import moment from 'moment';
import { TypeProduct, TypeProductEnum } from '../../../interfaces/product';

interface Props {
    loading: boolean;
    product: {
        name: string;
        image: string;
        price: number;
        stock: number | undefined;
        seller: string;
        reservations: string[]
    },
    typeProduct?: TypeProduct;
}

export const Product = (props: Props) => {

    const { loading, product, typeProduct } = props;
    const navigate = useNavigate();
    const onNavigateReturn = () => {
        navigate('/');
    }

    const tileDisabled = (args: {date: Date}):boolean => {
        const value = moment(args.date).format('L');
        return product.reservations.some((e) => e === value);
    }

    return (
        <div className="ms-5 bg-mutted  mt-5" >

            {loading && ('...loading')}

            { !loading && (
                <div>
                    <div className="cursor-pointer" onClick={onNavigateReturn} id="icon-return">
                        <FontAwesomeIcon icon={faArrowLeft} />
                        <span className="ms-2">Regresar</span>
                    </div>
                    <div id="container" className="mt-4 d-flex">
                        <div  id="image"  >
                            <img height={300} width={300} src={product?.image} alt="" />
                        </div>
                        <div className="ms-4">
                            <div className="h3">
                                <span>{ product?.name }</span>
                            </div>
                            <div className="col-6 mb-3">
                                <p className="justify fw-normal h5">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, enim in! Earum dolores quia consequuntur aliquam quae sunt animi rerum accusantium officiis laudantium maiores eius veritatis, vitae delectus non aperiam.
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate asperiores, delectus officia voluptatum numquam velit! Quod, sapiente eos. Dicta delectus dolore libero ipsa commodi quo eveniet et neque, nulla maiores?
                                   
                                </p>
                                <strong>Vendedor: { product.seller }</strong>

                            </div>
                            <div className="d-flex">
                                <div className="rectangle">
                                    <div>
                                            <div className='title'>
                                                Precio
                                            </div>
                                            <div className="d-flex justify-content-center">
                                                $ {product?.price}
                                            </div>
                                    </div>
                                </div>

                                {typeProduct === TypeProductEnum.SIMPLE && (

                                    <div className="rectangle ms-3">
                                        <div>
                                                <div className='title'>
                                                    Inventario
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    {product?.stock}
                                                </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>

                    </div>

                    { typeProduct !== TypeProductEnum.SIMPLE && (
                        <div className='mt-5 '>
                            <h3>Calendario</h3>
                            <p className='justify fw-normal h5'>Los días que están deshabilitados, son días ya reservados</p>
                            <Calendar 
                                tileDisabled={tileDisabled}
                            onChange={() => {}} value={new Date()} />
                        </div>
                    )}
                    
                </div>
            )}
            
            
        </div>
    );
}


Product.defaultProps = {
    typeProduct: TypeProductEnum.SIMPLE,
}