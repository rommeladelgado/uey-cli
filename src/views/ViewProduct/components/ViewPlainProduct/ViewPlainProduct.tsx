import {  useParams } from "react-router-dom";

import { useQuery } from "@apollo/client";
import { GET_PLAIN_PRODUCT_BY_ID } from '../../../../queries/plainProduct';
import { useEffect, useState } from "react";
import { PlainProduct } from "../../../../interfaces/product";

import { Product } from "../Product";


export const ViewPlainProduct  = () => {

    const { productId } = useParams();
    
    const [product, setProduct] = useState<PlainProduct | any>({});
    

    const { data,  loading } = useQuery(GET_PLAIN_PRODUCT_BY_ID, {
        variables: { id: Number(productId) }
    });

    useEffect(() => {
        if (data) {
            const pro =  data.getPlainProductById as PlainProduct;
            setProduct({...pro});
        }
    }, [data]);

    return (
        <Product 
            loading={loading}
            product={{
                ...product
            }}
        />
    );
};