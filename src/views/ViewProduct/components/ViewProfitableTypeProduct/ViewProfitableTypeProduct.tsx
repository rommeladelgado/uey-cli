import { useQuery } from "@apollo/client";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ProfitableTypeProduct, TypeProductEnum } from "../../../../interfaces/product";
import { ProfitableReservation } from "../../../../interfaces/profitableReservation";
import { GET_PROFITABLE_TYPE_PRODUCT_BY_ID } from "../../../../queries/profitableTypeProduct";
import { Product } from "../Product";
import moment from "moment";

export const ViewProfitableTypeProduct  = () => {

    const { productId } = useParams();
    
    const [product, setProduct] = useState<ProfitableTypeProduct | any>({reservations: []});
    

    const { data,  loading } = useQuery(GET_PROFITABLE_TYPE_PRODUCT_BY_ID, {
        variables: { id: Number(productId) }
    });

    useEffect(() => {
        if (data) {
            const pro =  data.getSpaceTypeProductById as ProfitableTypeProduct;
            setProduct({...pro} );
        }
    }, [data]);

    console.log(product.reservations);
    return (
        <Product 
            loading={loading}
            product={{
                ...product,
                reservations: product?.reservations.map((val: ProfitableReservation) => moment(val.reservationDate).format('L'))
            }}
            typeProduct={TypeProductEnum.ARRENDABLE}
        />
    );
};