import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Product } from "../Product";
import { SpaceTypeProduct, TypeProductEnum } from "../../../../interfaces/product";
import moment from "moment";
import { useQuery } from "@apollo/client";
import { SpaceReservation } from "../../../../interfaces/spaceReservation";
import { GET_SPACE_TYPE_PRODUCT_BY_ID } from "../../../../queries/spaceTypeProduct";

export const ViewSpaceTypeProduct  = () => {

    const { productId } = useParams();
    
    const [product, setProduct] = useState<SpaceTypeProduct | any>({reservations: []});
    

    const { data,  loading } = useQuery(GET_SPACE_TYPE_PRODUCT_BY_ID, {
        variables: { id: Number(productId) }
    });

    useEffect(() => {
        if (data) {
            const pro =  data.getSpaceTypeProductById as SpaceTypeProduct;
            setProduct({...pro} );
        }
    }, [data]);

    console.log(product.reservations);
    return (
        <Product 
            loading={loading}
            product={{
                ...product,
                reservations: product?.reservations.map((val: SpaceReservation) => moment(val.reservationDate).format('L'))
            }}
            typeProduct={TypeProductEnum.ARRENDABLE}
        />
    );
};