import { useMemo } from "react";
import { PlainProduct, ProfitableTypeProduct, SpaceTypeProduct } from "../interfaces/product";
import { TypeProduct } from "../interfaces/product";
import { EventContext } from "./eventContext"


interface Props {
    children: any,
    page:number,
    loading: boolean,
    total: number,
    rowsPerPage: number,
    typeProduct: TypeProduct,
    search: string,
    plainProducts: PlainProduct[],
    profitableTypeProducts: ProfitableTypeProduct[],
    spaceTypeProducts: SpaceTypeProduct[],
    selectPage: Function,
    onChangeTypeProduct: Function,

    onChangeSearch: Function,
}
export const EventProvider = (props: Props) => {

    const { 
        page,
        loading,
        total,
        rowsPerPage,
        typeProduct,
        search,
        plainProducts,
        profitableTypeProducts,
        spaceTypeProducts,
        selectPage,
        onChangeTypeProduct,

        onChangeSearch,
        children
     } = props;

    const value = useMemo(() => ({
        page,
        loading,
        total,
        rowsPerPage,
        typeProduct,
        search,
        plainProducts,
        profitableTypeProducts,
        spaceTypeProducts,
        selectPage,
        onChangeTypeProduct,

        onChangeSearch
    }), [
        page,
        loading,
        total,
        rowsPerPage,
        typeProduct,
        search,
        plainProducts,
        profitableTypeProducts,
        spaceTypeProducts,
        selectPage,
        onChangeTypeProduct,

        onChangeSearch
    ]);
    return (
        <EventContext.Provider value={value}>
            {children}
        </EventContext.Provider>
    )
}