import React from "react";
import { PlainProduct, ProfitableTypeProduct, SpaceTypeProduct } from "../interfaces/product";
import { TypeProduct } from "../interfaces/product";

export interface EventContextModel {
    page:number,
    loading: boolean,
    total: number,
    rowsPerPage: number,
    typeProduct: TypeProduct,
    search: string,
    plainProducts: PlainProduct[],
    profitableTypeProducts: ProfitableTypeProduct[],
    spaceTypeProducts: SpaceTypeProduct[],
    selectPage: Function,
    onChangeTypeProduct: Function,

    onChangeSearch: Function,
}
export const EventContext = React.createContext<EventContextModel>({
    loading: false,
    page: 0,
    search: '',
    total: 0,
    plainProducts: [],
    profitableTypeProducts:[],
    spaceTypeProducts: [],
    typeProduct: 'simple',
    rowsPerPage: 0,
    onChangeSearch: () => {},
    onChangeTypeProduct: () => {},
    selectPage: () => {},

});