import { gql } from '@apollo/client';

export const GET_ALL_PROFITABLE_TYPE_PRODUCTS = gql`
query GetAll($page: Int!, $rowsPerPage: Int!, $search: String!){
    getAllProfitableProducts(page:$page, rowsPerPage: $rowsPerPage, search: $search) {
      count
      list {
        id
        name
        description
        image
        price
        seller
		    reservations {
                id
				reservationDate
				rentType
            }
      }
    }
  }
`

export const GET_PROFITABLE_TYPE_PRODUCT_BY_ID = gql`
query GetAll($id: Int!){
  getProfitableById(id: $id) {
      id
      name
      description
      price
      seller
      image
      reservations {
        id
        reservationDate
      }
    }
  }
`