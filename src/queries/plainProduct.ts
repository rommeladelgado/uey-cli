import { gql } from '@apollo/client';

export const GET_ALL_PLAIN_PRODUCTS = gql`
    query GetAllPlainProducts($page: Int!, $rowsPerPage: Int!, $search: String!){
        getAllPlainProducts(page:$page, rowsPerPage: $rowsPerPage, search: $search) {
            count
            list {
                id
                name
                description
                image
                seller
                price
                stock
            }
        }
  }
`

export const GET_PLAIN_PRODUCT_BY_ID = gql`
    query getById($id: Int!){
        getPlainProductById(id:$id) {
            id
            name
            description
            image
            seller
            price
            stock
        }
  }
`