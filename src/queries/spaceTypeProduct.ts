import { gql } from '@apollo/client';

export const GET_ALL_SPACE_TYPE_PRODUCTS = gql`
query GetAll($page: Int!, $rowsPerPage: Int!, $search: String!){
    getAllSpaceProducts(page:$page, rowsPerPage: $rowsPerPage, search: $search) {
      count
      list {
        id
        name
        description
        image
        seller
        latitude
        longitude
        price
        reservations {
          id
          reservationDate
        }
      }
    }
  }
`

export const GET_SPACE_TYPE_PRODUCT_BY_ID = gql`
query GetAll($id: Int!){
    getSpaceTypeProductById(id: $id) {
      id
      name
      description
      price
      seller
      image
      latitude
      longitude
      reservations {
          id
          reservationDate
      }
    }
  }
`